# tcd_rviz_plugins

Repository dedicated to the development of rviz plugins.


- Installation

You can clone this repository into your workspace using git clone:  
`$ git clone https://gitlab.com/railundergrads/tcd_rviz_plugins.git`  

- Building

Sourcing your workspace and runnning catkin_make will  
```
$ cd ~/your_workspace  
$ source devel/setup.bash
$ catkin_make
```

- Running the plugin

Once you've cloned the plugin into your workspace and run `catkin_make` you can launch Rviz and load the plugin in.  
`$ rosrun rviz rviz`  
If you get an error "Could not contact ROS master at..." make sure ROS is running. Running `roscore` in another terminal will launch ROS.

- Usage  
After launching Rviz, you can load the panel by going to Panels->Add New Panel and selecting the tcd_rviz_plugins Navigation panel  

![Alt text](images/choosing_plugin.png?raw=true "Choosing plugin")

Once the plugin has been loaded you enter the topic name your robot is subscribed to into the text bar.

![Alt text](images/Screen_loaded.png?raw=true "Plugin loaded")

Clicking into the blank box, you can now control the robot using the keyboard which sends Twist messages over the desired topic. (Here we have run `rostopic echo /cmd_vel` in another terminal window and we are going forwards and at an angle)

![Alt text](images/topic_echo.png?raw=true "Publishing command velocities")


The keyboard controls for the project can be seen here

![Alt text](images/Controls.png?raw=true "Keyboard mapping")
