#ifndef DRIVE_WIDGET_H
#define DRIVE_WIDGET_H

#include <QWidget>

namespace tcd_rviz_plugins
{

class DriveWidget: public QWidget
{
Q_OBJECT
public:
  DriveWidget( QWidget* parent = 0 );

  virtual void paintEvent( QPaintEvent* event );

  virtual void keyPressEvent( QKeyEvent* event );
  virtual void keyReleaseEvent( QKeyEvent* event );
  virtual void leaveEvent(QEvent* event );
  virtual QSize sizeHint() const { return QSize( 150, 150 ); }
  virtual int getControlMethod();
  virtual void setControlMethod(int method);

  const float DEFAULT_LINEAR_SCALE = 0.25;
  const float DEFAULT_ANGULAR_SCALE = 1;
Q_SIGNALS:
  void outputVelocity( float linear, float angular );
  void outputScalar( float linear, float angular );
  
protected:
  void sendVelocitiesFromKeyboard( int linear, int angular);
  void sendVelocitiesFromJoystick();
  void changeSpeedMultiplier(float linear, float angular);
  void stop();

  float linear_velocity_; // m/s
  float angular_velocity_; // radians/s
  float linear_scale_; // m/s
  float angular_scale_; // radians/s
};

} // end namespace tcd_rviz_plugins


#endif // DRIVE_WIDGET_H
