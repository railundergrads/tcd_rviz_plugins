#include <stdio.h>
#include <math.h>
#include <map>
#include <tuple>
#include <QPainter>
#include <QMouseEvent>

#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Joy.h>

// #include <teleop_twist_joy>
// #include "teleop_twist_joy/teleop_twist_joy.h"

#include "drive_widget.h"

namespace tcd_rviz_plugins
{

std::map<int, std::tuple<int, int>> move_bindings {
  {'I',{1,0}},
  {'O',{1,-1}},
  {'J',{0,1}},
  {'L',{0,-1}},
  {'U',{1,1}},
  {' ',{0,0}},
  {'k',{0,0}},
  {',',{-1,0}},
  {'.',{-1,1}},
  {'M',{-1,-1}},
  };

std::map<int, std::tuple<float, float>> speed_bindings {
  {'Q',{0.5, 0.5}}, //Add .5 to speed and angle
  {'W',{-0.5, -0.5}}, // Remove 0.5 from speed and angle
  {'A',{0.5,0}}, // Add 0.5 to speed
  {'S',{-0.5,0}}, // Remove 0.5 from speed
  {'Z',{0,0.5}}, // Add 0.5 to angle
  {'X',{0,-0.5}}, // Remove 0.5 from angle
  {'V', {0.0, 0.0}}, //RESET TO DEFAULTS
    };


enum control_methods{keyboard, joystick};
int count = 0;
int current_control_method = keyboard;

DriveWidget::DriveWidget( QWidget* parent )
  : QWidget( parent )
  , linear_velocity_( 0 )
  , angular_velocity_( 0 )
  , linear_scale_( DEFAULT_LINEAR_SCALE )
  , angular_scale_( DEFAULT_ANGULAR_SCALE )
{
  setFocusPolicy(Qt::StrongFocus); // Allows keyboard input into widget
  
}
QColor background;
QColor crosshair;
void DriveWidget::paintEvent( QPaintEvent* event )
{
  
  if( isEnabled() )
  {
    background = Qt::white;
    crosshair = Qt::black;
  }
  else
  {
    background = Qt::lightGray;
    crosshair = Qt::darkGray;
  }

  int w = width();
  int h = height();
  int size = (( w > h ) ? h : w) - 1;
  int hpad = ( w - size ) / 2;
  int vpad = ( h - size ) / 2;

  QPainter painter( this );
  painter.setBrush( background );
  painter.setPen( crosshair );

  painter.drawRect( QRect( hpad, vpad, size, size ));

  // painter.drawLine( hpad, height() / 3, hpad + size, height() / 3 ); //Horizontal line 1
  // painter.drawLine( hpad, height() * 2 / 3, hpad + size, height() * 2/ 3 ); //Horizontal line 2
  // //Need to find dimensions for these to be aligned correctly - x1 and x2 beign 1/3 or 2/3 of the screen, y1 and y2 being top and bottom of panel
  // painter.drawLine( width() *2, vpad, width()*2, vpad + size ); //Vertical line 1
  // painter.drawLine( width() * 2 / 3, vpad, width() * 2 / 3, vpad + size ); //Vertical line 2

}


void DriveWidget::keyPressEvent( QKeyEvent* event )
{
  if(current_control_method == keyboard)
  {
    int keyPressed = event->key();
    count = 0;
    if(keyPressed != 0)
    {
      if(move_bindings.count(keyPressed))
      {
        std::tuple<int, int> cmd_vel = move_bindings.find(keyPressed)->second;
        int linear_key = std::get<0>(cmd_vel);
        int angular_key = std::get<1>(cmd_vel);
        sendVelocitiesFromKeyboard(linear_key, angular_key);
      }
      if(speed_bindings.count(keyPressed))
      {
        std::tuple<int, int> speed_change = speed_bindings.find(keyPressed)->second;
        int linear_change = std::get<0>(speed_change);
        int angular_change = std::get<1>(speed_change);
        changeSpeedMultiplier(linear_change, angular_change);
      }     
    }
  }
}

void DriveWidget::leaveEvent( QEvent* event )
{
  stop();
}

void DriveWidget::keyReleaseEvent( QKeyEvent* event )
{
  while(count < 10)
  {
    count++;
    linear_velocity_ = (linear_scale_ / count);
    Q_EMIT outputVelocity( linear_velocity_, 0 );
  }
  stop();
}

void DriveWidget::sendVelocitiesFromKeyboard( int linear, int angular)
{  

  linear_velocity_ = linear * linear_scale_;
  angular_velocity_ = angular * angular_scale_;
  Q_EMIT outputVelocity( linear_velocity_, angular_velocity_ );

  update();
}

void DriveWidget::sendVelocitiesFromJoystick()
{
  if(current_control_method == joystick)
  {
    
  }
}

void DriveWidget::changeSpeedMultiplier(float linear, float angular)
{
  linear_scale_ += linear;
  angular_scale_ += angular;
  if(linear == 0.0 && angular == 0.0)
  {
    linear_scale_ = DEFAULT_LINEAR_SCALE;
    angular_scale_ = DEFAULT_ANGULAR_SCALE;
  }
  Q_EMIT outputScalar( linear_scale_, angular_scale_);
  update();
}


void DriveWidget::stop()
{
  linear_velocity_ = 0;
  angular_velocity_ = 0;
  Q_EMIT outputVelocity( linear_velocity_, angular_velocity_ );
  update();
}

int DriveWidget::getControlMethod()
{
  return current_control_method;
}

void DriveWidget::setControlMethod(int method)
{
  current_control_method = method;
}

} // end namespace tcd_rviz_plugins
