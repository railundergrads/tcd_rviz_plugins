#ifndef TEST_PLUGIN_H
#define TEST_PLUGIN_H

#ifndef Q_MOC_RUN
# include <ros/ros.h>

# include <rviz/panel.h>
#include <rviz/tool.h>
#include <rviz/default_plugin/tools/move_tool.h>
#endif

class QLineEdit;

namespace tcd_rviz_plugins
{

class DriveWidget;

class TestPlugin: public rviz::Panel
{
Q_OBJECT
public:
  TestPlugin( QWidget* parent = 0 );

  virtual void load( const rviz::Config& config );
  virtual void save( rviz::Config config ) const;

public Q_SLOTS:

  void setVel( float linear_velocity_, float angular_velocity_ );
  void setScalar( float linear_scale_, float angular_scale_ );
  void setTopic( const QString& topic );

protected Q_SLOTS:
  void sendVel();

  void updateTopic();

protected:
  DriveWidget* drive_widget_;

  QLineEdit* output_topic_editor_;

  QString output_topic_;

  ros::Publisher velocity_publisher_;

  ros::NodeHandle nh_;

  // rviz::MoveTool move_tool_;

  float linear_velocity_;
  float angular_velocity_;
  float linear_scale_;
  float angular_scale_;
};

} // end namespace tcd_rviz_plugins

#endif // TEST_PLUGIN_H
